Rails.application.routes.draw do
  devise_for :users
  root to: 'visitors#index'
  namespace :account do
    resources :users
  end
  resources :clients
  resources :file_matters do
  	get :autocomplete_client_name, :on => :collection
	end
  resources :lawyers
  resources :carl_entries
end
