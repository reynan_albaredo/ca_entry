require 'rails_helper'

describe 'Client' do
	it "cannot be created without email" do
		expect( Client.new email: nil ).to_not be_valid
	end

	it "cannot be created if email format is invalid" do
		expect( Client.new email: 'invalid_mail' ).to_not be_valid
	end
	
end