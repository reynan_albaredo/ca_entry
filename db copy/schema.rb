# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150308014748) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "carl_entries", force: :cascade do |t|
    t.date     "entry_date"
    t.integer  "category_id"
    t.integer  "user_id"
    t.string   "assigned_person"
    t.integer  "case_reference_id"
    t.string   "case_reference_number"
    t.integer  "client_id"
    t.string   "client_name"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "tin_of_client"
    t.text     "address"
    t.string   "bus_name_style"
    t.string   "terms_of_payment"
    t.string   "osc_pwd_id_no"
    t.string   "prepared_by"
    t.string   "approved_by"
  end

  create_table "carl_fields", force: :cascade do |t|
    t.date     "entry_date"
    t.string   "check_num"
    t.text     "particular"
    t.decimal  "amount",        precision: 15, scale: 2, default: 0.0, null: false
    t.integer  "carl_entry_id"
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
  end

  create_table "case_references", force: :cascade do |t|
    t.string   "year"
    t.string   "file_code"
    t.string   "volume"
    t.integer  "client_id"
    t.string   "title"
    t.string   "case_number"
    t.integer  "lawyer_id"
    t.date     "case_date"
    t.string   "currency_used",                          default: "Peso", null: false
    t.decimal  "amount",        precision: 15, scale: 2, default: 0.0,    null: false
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at",                                              null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "clients", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "contact_number"
    t.string   "position"
    t.text     "address"
    t.string   "contact_person"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "lawyers", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "middle_name"
    t.string   "mobile_number"
    t.string   "position"
    t.string   "email"
    t.string   "rate"
    t.string   "initials"
    t.string   "dollar_rate"
    t.string   "is_active",     default: "Yes"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "contact_number"
    t.text     "address"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
