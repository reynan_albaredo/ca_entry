class AddNewFieldsInEntryTable < ActiveRecord::Migration
  def change
  	add_column :carl_entries, :tin_of_client, :string
  	add_column :carl_entries, :address, :text
  	add_column :carl_entries, :bus_name_style, :string
  	add_column :carl_entries, :terms_of_payment, :string
  	add_column :carl_entries, :osc_pwd_id_no, :string
  	add_column :carl_entries, :prepared_by, :string
  	add_column :carl_entries, :approved_by, :string
  end
end
