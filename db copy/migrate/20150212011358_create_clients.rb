class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string   :name
	    t.string   :email
	    t.string   :contact_number
	    t.string   :position
	    t.text     :address
	    t.string   :contact_person
      t.timestamps null: false
    end
  end
end
