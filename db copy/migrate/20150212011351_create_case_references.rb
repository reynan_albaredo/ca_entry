class CreateCaseReferences < ActiveRecord::Migration
  def change
    create_table :case_references do |t|
    	t.string   :year
	    t.string   :file_code
	    t.string   :volume
	    t.integer  :client_id
	    t.string   :title
	    t.string   :case_number
	    t.integer  :lawyer_id
	    t.date   	 :case_date
	    t.string   :currency_used, :default => "Peso", :null => false
	    t.decimal  :amount,        :precision => 15, :scale => 2, :default => 0.0, :null => false
      t.timestamps null: false
    end
  end
end
