class Client < ActiveRecord::Base
	belongs_to :file_matter

	validates :email, presence: true, format: /@/
end
