class Lawyer < ActiveRecord::Base

	# has_many :assigned_lawyers, :dependent => :destroy 
  has_one :user
  has_many :case_entries
  
  def full_name 
  	"#{first_name} #{middle_name} #{last_name}"
  end
  
end
