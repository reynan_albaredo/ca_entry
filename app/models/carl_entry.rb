class CarlEntry < ActiveRecord::Base
	belongs_to :category
	has_many :carl_fields, dependent: :destroy
	accepts_nested_attributes_for :carl_fields
end
