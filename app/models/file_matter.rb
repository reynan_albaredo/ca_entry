class FileMatter < ActiveRecord::Base
	has_many :clients
	has_many :assigned_lawyers, :dependent => :destroy
  accepts_nested_attributes_for :assigned_lawyers, :allow_destroy => true
end
