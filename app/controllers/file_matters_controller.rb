class FileMattersController < ApplicationController
  autocomplete :client, :name
	before_filter :set_file_matter, only: [:show, :update, :destroy]

  def index
    @q = FileMatter.ransack(params[:q])
    @file_matters = @q.result.paginate(:page => params[:page], :per_page => 20)
    # render json: @file_matters
  end

  def new
    @file_matter = FileMatter.new
    @clients = Client.all
    @file_matter.assigned_lawyers.build
  end
  
  def edit
    @file_matter = FileMatter.find(params[:id])
  end

  def create
    @file_matter = FileMatter.new(file_matter_params)

    if @file_matter.save
      flash[:success] = 'Case Reference was successfully created.'
      redirect_to file_matters_path
    else
      flash[:error] = 'An error occured while creating new file_matter.'
      render 'new'
    end
  end

  def update
    if @file_matter.update(file_matter_params)
      flash[:success] = 'Case Reference was successfully updated.'
      redirect_to file_matters_path
    else
      flash[:error] = 'An error occured while creating new file_matter.'
      render 'new'
    end
  end
  
  def destroy
    @file_matter.destroy!
    flash[:success] = 'Case Reference has been deleted.' 
    redirect_to file_matters_path
  end

  private

    def set_file_matter
      @file_matter = FileMatter.find(params[:id])
    end
      
    def file_matter_params
      params.require(:file_matter).permit(:year, :file_code, :volume, :client_id, :title, :case_number, :case_date, :amount, :currency_used,
        assigned_lawyers_attributes: [:file_matter_id, :client_id, :lawyer_id, :amount, :file_matter_pricing] )
    end

end
