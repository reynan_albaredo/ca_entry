class CarlEntriesController < ApplicationController


	before_filter :set_carl_entry, only: [:show, :update, :destroy]

  def index
    @carl_entries = CarlEntry.paginate(:page => params[:page], :per_page => 20)
    # render json: @carl_entries
  end

  def new
    @carl_entry = CarlEntry.new
    @clients = Client.all
  end
  
  def edit
    @carl_entry = CarlEntry.find(params[:id])
  end

  def show
    @carl_entry = CarlEntry.find(params[:id])
    @carl_fields = CarlField.where(carl_entry_id: @carl_entry.id)
    respond_to do |format|
      format.html
      format.pdf do
        render :pdf         => "Reimbursement Form",
              :orientation  => 'Portrait',
              :page_width   => '13in',
              :margin => {:top       => 1,
                           :bottom   => 1}
      end
    end
  end

  def create
    @carl_entry = CarlEntry.new(carl_entry_params)

    if @carl_entry.save
      flash[:success] = 'Case Reference was successfully created.'
      redirect_to carl_entries_path
    else
      flash[:error] = 'An error occured while creating new carl_entry.'
      render 'new'
    end
  end

  def update
    if @carl_entry.update(carl_entry_params)
      flash[:success] = 'Case Reference was successfully updated.'
      redirect_to carl_entries_path
    else
      flash[:error] = 'An error occured while creating new carl_entry.'
      render 'new'
    end
  end
  
  def destroy
    @carl_entry.destroy!
    flash[:success] = 'Case Reference has been deleted.' 
    redirect_to carl_entries_path
  end

  private

    def set_carl_entry
      @carl_entry = CarlEntry.find(params[:id])
    end
      
    def carl_entry_params
      params.require(:carl_entry).permit(:entry_date, :category_id, :user_id, :assigned_person, :file_matter_id, 
        :case_reference_number, :client_id, :client_name, :tin_of_client, :address, :bus_name_style, :terms_of_payment, 
        :osc_pwd_id_no, :prepared_by,
        :approved_by, carl_fields_attributes: [:entry_date, :check_num, :particular, :amount, :carl_entry_id] )
    end
end
