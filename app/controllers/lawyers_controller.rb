class LawyersController < ApplicationController
	before_filter :set_lawyer, only: [:show, :update, :destroy]

  def index
    @lawyers = Lawyer.paginate(:page => params[:page], :per_page => 20)
    # render json: @lawyers
  end

  def new
    @lawyer = Lawyer.new
    @clients = Client.all
  end
  
  def edit
    @lawyer = Lawyer.find(params[:id])
  end

  def create
    @lawyer = Lawyer.new(lawyer_params)

    if @lawyer.save
      flash[:success] = 'Case Reference was successfully created.'
      redirect_to lawyers_path
    else
      flash[:error] = 'An error occured while creating new lawyer.'
      render 'new'
    end
  end

  def update
    if @lawyer.update(lawyer_params)
      flash[:success] = 'Case Reference was successfully updated.'
      redirect_to lawyers_path
    else
      flash[:error] = 'An error occured while creating new lawyer.'
      render 'new'
    end
  end
  
  def destroy
    @lawyer.destroy!
    flash[:success] = 'Case Reference has been deleted.' 
    redirect_to lawyers_path
  end

  private

    def set_lawyer
      @lawyer = Lawyer.find(params[:id])
    end
      
    def lawyer_params
      params.require(:lawyer).permit(:first_name, :last_name, :middle_name, :mobile_number, :position, :email, :rate, :initials, :dollar_rate, :is_active)
    end

end
