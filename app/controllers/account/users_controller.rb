class Account::UsersController < AccountController
	before_filter :set_user, only: [:show, :update, :destroy]

  def index
    @q = User.ransack(params[:q])
    @users =  @q.result.paginate(:page => params[:page], :per_page => 20)
    # render json: @users
  end

  def new
    @user = User.new
  end
  
  def edit
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(user_params)

    if @user.save
      flash[:success] = 'User was successfully created.'
      redirect_to account_users_path
    else
      flash[:error] = 'An error occured while creating new user.'
      render 'new'
    end
  end

  def update
    if @user.update(user_params)
      flash[:success] = 'User was successfully updated.'
      redirect_to account_users_path
    else
      flash[:error] = 'An error occured while creating new user.'
      render 'new'
    end
  end
  
  def destroy
    @user.destroy!
    flash[:success] = 'User has been deleted.' 
    redirect_to account_users_path
  end

  private

    def set_user
      @user = User.find(params[:id])
    end
      
    def user_params
      params.require(:user).permit(:email, :name, :password, :contact_number, :address)
    end
end
