class ClientsController < ApplicationController
	before_filter :set_client, only: [:show, :update, :destroy]

  def index
    @q = Client.ransack(params[:q])
    @clients = @q.result.paginate(:page => params[:page], :per_page => 20)
    # render json: @clients
  end

  def new
    @client = Client.new
  end
  
  def edit
    @client = Client.find(params[:id])
  end

  def create
    @client = Client.new(client_params)

    if @client.save
      flash[:success] = 'Client was successfully created.'
      redirect_to clients_path
    else
      flash[:error] = 'An error occured while creating new client.'
      render 'new'
    end
  end

  def update
    if @client.update(client_params)
      flash[:success] = 'Client was successfully updated.'
      redirect_to clients_path
    else
      flash[:error] = 'An error occured while creating new client.'
      render 'new'
    end
  end
  
  def destroy
    @client.destroy!
    flash[:success] = 'Client has been deleted.' 
    redirect_to clients_path
  end

  private

    def set_client
      @client = Client.find(params[:id])
    end
      
    def client_params
      params.require(:client).permit(:name, :email, :address, :contact_number, :position, :contact_person)
    end
end
