module ApplicationHelper
	def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def active_class(current_controller)
    params[:controller] == current_controller ? "active" : nil
  end

  # def tally(sample)
  #     words = sample.split(" ")
  #     words.inject({}) do |counter, item|
  #       counter[item] ||= 0
  #       counter[item]  += 1
  #       counter
  #     end
  # end

  # def filter(phrase, blacklist)
  #   words = []
  #   phrase.split.each do |chk|
  #     blacklist.include?(chk.downcase) ? words << '*********' : words << chk
  #   end
  #   words.join(" ")
  # end

  # def link_to_users(message, domain)
  #   # /@/.match('@mother')
  #   var = message.split.each do |msg|
  #     x = /@/.match(msg)
  #     x.nil? ? msg : msg.gsub!(msg, "<a href='#{domain}#{msg.gsub(/^./, "")}'>#{msg.gsub(/^./, "")}</a>")
  #   end
  #   var.join(" ")
  # end

end