# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150720212016) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "assigned_lawyers", force: :cascade do |t|
    t.integer  "lawyer_id"
    t.integer  "file_matter_id"
    t.integer  "client_id"
    t.datetime "created_at",                                                 null: false
    t.datetime "updated_at",                                                 null: false
    t.decimal  "amount",              precision: 15, scale: 2, default: 0.0, null: false
    t.decimal  "file_matter_pricing", precision: 15, scale: 2, default: 0.0, null: false
  end

  create_table "calls", force: :cascade do |t|
    t.string   "called_number",     limit: 255
    t.integer  "number_of_minutes"
    t.integer  "call_amounts"
    t.string   "client",            limit: 255
    t.date     "call_date"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "client_id"
  end

  create_table "carl_entries", force: :cascade do |t|
    t.date     "entry_date"
    t.integer  "category_id"
    t.integer  "user_id"
    t.string   "assigned_person"
    t.integer  "case_reference_id"
    t.string   "case_reference_number"
    t.integer  "client_id"
    t.string   "client_name"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "tin_of_client"
    t.text     "address"
    t.string   "bus_name_style"
    t.string   "terms_of_payment"
    t.string   "osc_pwd_id_no"
    t.string   "prepared_by"
    t.string   "approved_by"
    t.string   "request_for"
    t.string   "charge_to"
    t.string   "payment_method"
    t.string   "requested_by"
    t.datetime "request_date"
    t.datetime "approved_date"
  end

  create_table "carl_fields", force: :cascade do |t|
    t.date     "entry_date"
    t.string   "check_num"
    t.text     "particular"
    t.decimal  "amount",        precision: 15, scale: 2, default: 0.0, null: false
    t.integer  "carl_entry_id"
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
  end

  create_table "case_entries", force: :cascade do |t|
    t.date     "entry_date"
    t.text     "work_particulars"
    t.integer  "client_id"
    t.string   "file_matter_id",                 limit: 255
    t.string   "time_spent_from",                limit: 255
    t.string   "time_spent_to",                  limit: 255
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
    t.string   "file_matter_case",               limit: 255
    t.integer  "lawyer_id"
    t.text     "case_title"
    t.integer  "user_id"
    t.boolean  "create_multiple_lawyer_entries"
    t.string   "client_name",                    limit: 255
    t.string   "remove_from_billing",            limit: 255, default: "No"
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "clients", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.string   "email",          limit: 255
    t.string   "contact_number", limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "position",       limit: 255
    t.text     "address"
    t.string   "contact_person", limit: 255
  end

  create_table "file_matters", force: :cascade do |t|
    t.string   "year",          limit: 255
    t.string   "file_code",     limit: 255
    t.string   "volume",        limit: 255
    t.string   "client_id",     limit: 255
    t.string   "title",         limit: 255
    t.string   "case_number",   limit: 255
    t.string   "lawyer_id",     limit: 255
    t.string   "case_date",     limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "currency_used", limit: 255,                          default: "Peso", null: false
    t.decimal  "amount",                    precision: 15, scale: 2, default: 0.0,    null: false
    t.decimal  "case_pricing",              precision: 15, scale: 2, default: 0.0,    null: false
    t.boolean  "fixed_rate"
  end

  create_table "lawyers", force: :cascade do |t|
    t.string   "first_name",    limit: 255
    t.string   "last_name",     limit: 255
    t.string   "middle_name",   limit: 255
    t.string   "mobile_number", limit: 255
    t.string   "position",      limit: 255
    t.string   "email",         limit: 255
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.string   "rate",          limit: 255
    t.string   "initials",      limit: 255
    t.string   "dollar_rate",   limit: 255
    t.string   "is_active",     limit: 255, default: "Yes"
  end

  create_table "notifications", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "notification_title", limit: 255
    t.text     "details"
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.boolean  "notified"
  end

  create_table "printouts", force: :cascade do |t|
    t.integer  "ref_no"
    t.date     "entry_date"
    t.string   "case_title",       limit: 255
    t.string   "client",           limit: 255
    t.string   "document_name",    limit: 255
    t.integer  "num_page"
    t.integer  "num_copy"
    t.string   "paper_size",       limit: 255
    t.datetime "created_at",                                                          null: false
    t.datetime "updated_at",                                                          null: false
    t.integer  "client_id"
    t.integer  "file_matter_id"
    t.string   "file_matter_case", limit: 255
    t.integer  "lawyer_id"
    t.decimal  "times_amount",                 precision: 15, scale: 2, default: 0.0, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "",    null: false
    t.string   "encrypted_password",     limit: 255, default: "",    null: false
    t.string   "username",               limit: 255
    t.string   "name",                   limit: 255
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.string   "role",                   limit: 255
    t.integer  "lawyer_id"
    t.string   "is_active",              limit: 255, default: "Yes"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
