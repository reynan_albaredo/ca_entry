class AddFieldsOnTheTable < ActiveRecord::Migration
  def change
  	add_column :carl_entries, :request_for, :string
  	add_column :carl_entries, :charge_to, :string
  	add_column :carl_entries, :payment_method, :string
  	add_column :carl_entries, :requested_by, :string
  	add_column :carl_entries, :request_date, :datetime
  	add_column :carl_entries, :approved_date, :datetime
  end
end
