class CreateCarlEntries < ActiveRecord::Migration
  def change
    create_table :carl_entries do |t|
    	t.date :entry_date
    	t.integer :category_id
    	t.integer :user_id
    	t.string  :assigned_person
    	t.integer :case_reference_id
    	t.string  :case_reference_number
    	t.integer :client_id
    	t.string  :client_name
      t.timestamps null: false
    end
  end
end
