class CreateCarlFields < ActiveRecord::Migration
  def change
    create_table :carl_fields do |t|
    	t.date :entry_date
    	t.string :check_num
    	t.text :particular
    	t.decimal  :amount, :precision => 15, :scale => 2, :default => 0.0, :null => false
    	t.integer :carl_entry_id
      t.timestamps null: false
    end
  end
end
